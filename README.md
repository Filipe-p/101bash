# This is about Linux and Bash

This lecture is on linux and bash concepts. 

### Linux flavours and Package Managers

Manages packages - basically installs software. 
Like an app store fore the terminal. 

#### Debian distributions (ubuntu)
- `apt-get`
- `aptitude`
- `dpkg`

#### RHEL (red hat systems)
- `yum`
- `dnf`
- `rpm`

#### Mac 
- brew install



## Syntax of linux

```
command [options] [arguments]
```


Examples of commands:
- ls
  - ls -a *.txt
- pwd
- ps
- touch <name of file>
- mv
- copy
- rm



## 3 Standard ouputs & inputs AND Ridirection
These are the standard inputs and output:
- Standrad input (stdin) 
- Standard output (stdout) 
- Standard error (stderr)

This is how you redirect: 
- `>` truncates -- that means it will destroy what was there previously
- `>>` appends -- that means it will add to the bottom of the exisitiong information
- pipe the outcome (1 or 2) into the input of another function (0) using `|`



#### Standrad input (stdin) 0
- Standrad input (stdin) is controlled with number 0


#### Standard output (stdout) 1
This is the standard response given by a program on bash that did not cause and erro. This is for example the output of an ´ls´in a folder with files.

- Standard output (stdout) is controlled with number 1

Examples
```
# the one in this case is redundant but it works 
$ ls 1> example.txt

# if it find a file or folder called pabloescobar it will print it in text.txt
# however, if it doesn't find it, the standerr (2) will still print to screen|/terminal
$ find pabloescobar 1> text.txt
> find: pabloescobar: No such file or directory
```

#### Standard error (stderr) 2
When you try to use a program and it causes an error, this output comes as a standard error! It is controleed using number2  of redirection.

- Standard error (stderr) is controlled with number 2

Examples
```
# In this case, it will not breask and the file example will be empty
$ ls 2> example.txt

# if it find a file or folder called pabloescobar it will print it in text.txt
# however, if it doesn't find it, the standerr (2) will still print to screen|/terminal
$ find pabloescobar 2> text.txt
> find: pabloescobar: No such file or directory
```


### Environment Variables 

An environment is a computer / Machine. 

Any program you run, like python or JS, runs on top of this environment. 

So if you set Environment variables, you might be able to caputre them inside other programs. 

This is perticularly important for some operations, such as:
- Passing secretes and keys 
- passing IP and identifying machines
- making DB and app servers know about each other


You can set a variable that exist ONLY in that session by simply:

```Bash
#setting variable
$ MYVAR=HELLO

Calling Variable 
$ echo $MYVAR
> HELLO

```

Once you close the terminal OR start a child process you'll not have said variable.

#### Child Process & Export

This is a program that runs from your existing session. 
For example executing a bash file or a python file. 

To pass along variable to child processess they MUST explicitly be EXPORTED using the key word `export`.

```bash
# To see that child process does not have access to a variable
# set your variable and then export
$ export MYVAR=CAT

$ bash ./bash_tests.sh

```

To give access to child process, you must export your VAR


#### How to make Environment variables presistant

Meaning you can close the terminal and open it and it will always exist. 

You need to understand the PATH of the terminal. 

And then you need to set the variable in the files what are read in said path. And export if you want them available in other programs. 


```bash

## Bash --- what files?


# ZSH profiles --- what Files? 

```


